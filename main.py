#!/usr/bin/python3

from mpi4py import MPI
from collections import namedtuple
from time import sleep
from objects import Bee, Board, Worker, Food, Navigator

size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()
name = MPI.Get_processor_name()
info = MPI.Status()
comm = MPI.COMM_WORLD

BoardDimensions = namedtuple('BoardDimensions', 'x y')
Node = namedtuple('Node', 'location parent g h weight')
Point = namedtuple('Point', 'x y')

if __name__ == '__main__':

    numOfHives = 1
    numOfFoodNodes = (size - numOfHives) / 4
    numOfBees = numOfFoodNodes
    numOfWorkers = size - numOfFoodNodes - numOfBees - numOfHives

    hiveRank = 0
    foodRanks = range((hiveRank+1),(numOfFoodNodes+1))
    beeRanks = range(foodRanks[len(foodRanks)-1]+1,foodRanks[len(foodRanks)-1] + numOfBees + 1)
    workerRanks = range(beeRanks[len(beeRanks)-1]+1, size)

    worldState = None
    currentPosition = None
    hiveLocation = None

    if rank == 0:
        print("number of hives  : {}".format(numOfHives))
        print("number of foods  : {}".format(numOfFoodNodes))
        print("number of workers: {}".format(numOfWorkers))
        print("number of bees   : {}".format(numOfBees))
        print("Hive Rank: {}".format(hiveRank))
        print("Food Rank: {}".format(foodRanks))
        print("Bees Rank: {}".format(beeRanks))
        print("Work Rank: {}".format(workerRanks))

        hive = Board()

        for i in range(1, size):
            if i <= numOfFoodNodes:
                hive.add_resource()
            elif i > numOfFoodNodes and i <= (numOfBees + numOfFoodNodes):
                hive.add_bee()
            else:
                hive.add_worker_bee()

        # Generate a random board
        worldState = comm.bcast(hive.board, root=0)
        hiveLocation = comm.bcast(hive.hiveLocation, root=0)

        # Send other nodes their locations
        currentPosition = comm.scatter([hiveLocation] + hive.Food + hive.Bees + hive.WorkerBees, root=0)
        print("Everything initiated")

        while True:
            updatedWorldState = comm.bcast(worldState, root=0)

            # small sleep to help display
            sleep(.15)
            # print('\x1b[2J')
            print(hive)

            for i, request in enumerate(comm.gather(root=0)):

                if request != None:
                    # Currently assuming request is a tuple of (Request Type, Argument)
                    if request[0] == 'worker_move':
                        if request[1] != None:
                            # Update the position of the bee
                            if hive.is_clear(request[1]):
                                hive.move_to('%', hive.WorkerBees[i - numOfFoodNodes - numOfHives - numOfBees], request[1])
                                hive.WorkerBees[i - numOfFoodNodes - numOfHives - numOfBees] = request[1]
                                comm.send(True, dest=i, tag=1)
                                continue
                    if request[0] == 'bee_move':
                        if request[1] != None:
                            # Update the position of the bee
                            if hive.is_clear(request[1]):
                                hive.move_to('*', hive.Bees[i - numOfFoodNodes - numOfHives], request[1])
                                hive.Bees[i - numOfFoodNodes - numOfHives] = request[1]
                                comm.send(True, dest=i, tag=1)
                                continue
                    elif request[0] == 'out_of_food':
                        # Update the position of the bee
                        # Respond to the node telling it the movement was successful

                        #hive.move_to(hive.Bees[i - numOfHives], )
                        #hive.Food[i - numOfHives] = request[1]
                        comm.send(True, dest=i, tag=1)
                        continue

                comm.send(False, dest=i, tag=1)

    elif rank > 0 and rank <= numOfFoodNodes:

        # Get board and positional info
        worldState = comm.bcast(worldState, root=0)
        currentPosition = comm.scatter(currentPosition, root=0)
        hiveLocation = comm.bcast(hiveLocation, root=0)

        # Instantiate self with board and position
        food = Food(rank, worldState, currentPosition, 10)

        while True:

            food.board = comm.bcast(worldState, root=0)

            # If we get a request for food
            for i in beeRanks:
                if comm.Iprobe(source=i, tag=3):
                    message = comm.recv(source=i, tag=3)
                    req = comm.isend('give_food', dest=i, tag=4)
                    req.wait()
                    food.amountOfRemainingResource -= 1
                    print("Food left: {}".format(food.amountOfRemainingResource))

            if food.amountOfRemainingResource == 0:
                print("Food died")
                # Update root with desired pos, get back success or fail
                message = 'out_of_food'
            else:
                message = 'food_check_in'

            request = comm.gather(message, root=0)
            result = comm.recv(source=0, tag=1)

            if not result:
                continue

    elif rank > numOfFoodNodes and rank <= (numOfBees + numOfFoodNodes):
        # Get board and positional info
        worldState = comm.bcast(worldState, root=0)
        currentPosition = comm.scatter(currentPosition, root=0)

        # Instantiate self with board and position
        bee = Bee(rank, worldState, currentPosition, True)
        hiveLocation = comm.bcast(hiveLocation, root=0)
        bee.goalPosition = hiveLocation

        while True:
            # Get most updated board
            bee.board = comm.bcast(worldState, root=0)

            # Determine the next position to move to
            next_pos = bee.navigate(bee.goalPosition)

            # Update Hive
            request = comm.gather(('bee_move', next_pos), root=0)
            if comm.recv(source=0, tag=1):
                # Could move
                bee.currentPosition = next_pos

            # Get food location from worker bees, go to it if bee is not already doing something
            for i in workerRanks:
                if comm.Iprobe(source=i, tag=2):
                    message = comm.recv(source=i, tag=2)
                    #print("Distance to bee {}".format (Navigator.distance(message[1],bee.currentPosition)))
                    if Navigator.distance(message[1],bee.currentPosition) <= 30:
                            bee.goalPosition = message[2]


            # Get a bit of food
            if bee.goalPosition != hiveLocation and not bee.isCarryingFood:
                #print("Distance to food: {} ".format(Navigator.distance(bee.currentPosition, bee.goalPosition)))
                if Navigator.distance(bee.currentPosition, bee.goalPosition) <= 30:
                    for i in foodRanks:
                        #print("Sent food request")
                        req = comm.isend('give', dest=i, tag=3)
                        req.wait()

                        # If we get a reply from any food
                        if comm.Iprobe(source=i, tag=4):
                            message = comm.recv(source=i, tag=4)
                            bee.isCarryingFood = True
                            bee.goalPosition = hiveLocation
                            req.wait()

            # deliver food
            if bee.isCarryingFood:
                if Navigator.distance(bee.currentPosition, bee.goalPosition) <= 30:
                    bee.isCarryingFood = False
                    bee.goalPosition = hiveLocation

    else:
        # Get board and positional info
        worldState = comm.bcast(worldState, root=0)
        currentPosition = comm.scatter(currentPosition, root=0)

        # Instantiate self with board and position
        workerBee = Worker(rank, worldState, currentPosition, 'wandering', False)
        workerBee.goalPosition = comm.bcast(hiveLocation, root=0)

        while True:

            # Get most updated board
            workerBee.board = comm.bcast(worldState, root=0)
            next_pos = None

            # Determine the next position to move to
            if workerBee.mode == 'wandering':
                next_pos = workerBee.move_pos()
            elif workerBee.mode == 'navigating':
                message = ['food',workerBee.currentPosition, workerBee.foodPosition]
                next_pos = workerBee.navigate(workerBee.goalPosition)
                for i in beeRanks:
                    #print("Sent {} : {}".format(i,message))
                    req = comm.isend(message, dest=i, tag=2)
                    req.wait()

            # Check to see if we need to toggle the state of the bee
            if workerBee.isNearFood(2):
                workerBee.mode = 'navigating'
            elif workerBee.isNearHive(2):
                workerBee.mode = 'wandering'

            # Update Hive
            request = comm.gather(('worker_move', next_pos), root=0)
            if comm.recv(source=0, tag=1):
                # Could move
                workerBee.currentPosition = next_pos
                #print("Bee: {} is {} to ( {},{} )".format
                #(workerBee.rank,
                # workerBee.mode,
                # workerBee.currentPosition.y,
                # workerBee.currentPosition.x))